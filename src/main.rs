use std::{collections::HashMap, f32::consts::PI, mem::size_of, slice::from_raw_parts};

use psimple::Simple;
use pulse::{
    def::BufferAttr,
    sample::{Format, Spec},
    stream::Direction,
};
use rustfft::{
    num_complex::{Complex, ComplexFloat},
    FftPlanner,
};

const BUFFER_SIZE: usize = 512;
const SAMPLE_RATE: usize = 4096;
const SAMPLE_RATE_F32: f32 = SAMPLE_RATE as f32;
const BASE_FREQUENCIES: [f32; 12] = [
    16.35, 17.32, 18.35, 19.45, 20.60, 21.83, 23.12, 24.50, 25.96, 27.50, 29.14, 30.87,
];

#[derive(Debug, PartialEq, Eq, Hash)]
enum NoteLetter {
    C,
    Csh,
    D,
    Dsh,
    E,
    F,
    Fsh,
    G,
    Gsh,
    A,
    Ash,
    B,
}

impl TryFrom<BaseFrequency> for NoteLetter {
    type Error = ();

    fn try_from(value: BaseFrequency) -> Result<Self, Self::Error> {
        if !BASE_FREQUENCIES.contains(&value) {
            return Err(());
        }
        match value {
            16.35 => Ok(NoteLetter::C),
            17.32 => Ok(NoteLetter::Csh),
            18.35 => Ok(NoteLetter::D),
            19.45 => Ok(NoteLetter::Dsh),
            20.60 => Ok(NoteLetter::E),
            21.83 => Ok(NoteLetter::F),
            23.12 => Ok(NoteLetter::Fsh),
            24.50 => Ok(NoteLetter::G),
            25.96 => Ok(NoteLetter::Gsh),
            27.50 => Ok(NoteLetter::A),
            29.14 => Ok(NoteLetter::Ash),
            30.87 => Ok(NoteLetter::B),
            _ => Err(()),
        }
    }
}
#[derive(Debug, PartialEq, Eq, Hash)]
struct Note {
    note_letter: NoteLetter,
    octave: u8,
}

type Frequency = f32;
type BaseFrequency = f32;

impl TryFrom<usize> for Note {
    type Error = ();

    fn try_from(value: usize) -> Result<Self, Self::Error> {
        (value as f32).try_into()
    }
}

impl TryFrom<Frequency> for Note {
    type Error = ();

    fn try_from(mut value: Frequency) -> Result<Self, Self::Error> {
        if value < 16.35 {
            return Err(());
        }
        let mut divisions = 0;
        while value > 30.87 {
            value /= 2.0;
            divisions += 1;
        }

        let index = BASE_FREQUENCIES
            .iter()
            .map(|f| (f - value).abs())
            .enumerate()
            .min_by(|(_, i), (_, j)| i.partial_cmp(j).unwrap_or(std::cmp::Ordering::Equal))
            .unwrap()
            .0;

        let base_note = BASE_FREQUENCIES[index].try_into().unwrap();
        Ok(Note::new(base_note, divisions).unwrap())
    }
}

impl Note {
    fn new(note_letter: NoteLetter, octave: u8) -> Result<Self, ()> {
        use NoteLetter::*;
        if octave == 0 && (note_letter == A || note_letter == Ash || note_letter == B) {
            return Err(());
        }
        if octave > 8 {
            return Err(());
        }
        Ok(Note {
            note_letter,
            octave,
        })
    }

    fn to_hz(&self) -> f64 {
        let base_frequency = match self.note_letter {
            NoteLetter::C => 16.35,
            NoteLetter::Csh => 17.32,
            NoteLetter::D => 18.35,
            NoteLetter::Dsh => 19.45,
            NoteLetter::E => 20.60,
            NoteLetter::F => 21.83,
            NoteLetter::Fsh => 23.12,
            NoteLetter::G => 24.50,
            NoteLetter::Gsh => 25.96,
            NoteLetter::A => 27.50,
            NoteLetter::Ash => 29.14,
            NoteLetter::B => 30.87,
        };
        base_frequency * (self.octave as f64 + 1.0)
    }
}

fn main() {
    assert!(SAMPLE_RATE > BUFFER_SIZE);

    let spec = Spec {
        format: Format::FLOAT32NE,
        channels: 1,
        rate: SAMPLE_RATE as u32,
    };

    let attr = BufferAttr {
        maxlength: u32::MAX,
        tlength: u32::MAX,
        prebuf: u32::MAX,
        minreq: u32::MAX,
        fragsize: 100,
    };

    let server = Simple::new(
        None,
        "Chordula",
        Direction::Record,
        None,
        "Chordula",
        &spec,
        None,
        Some(&attr),
    )
    .unwrap();

    let mut buffer: Vec<u8> = vec![0; BUFFER_SIZE * size_of::<f32>()];
    let mut planner = FftPlanner::<f32>::new();
    let fft = planner.plan_fft_forward(SAMPLE_RATE);

    loop {
        server.read(&mut buffer).unwrap();

        let mut data: Vec<f32> =
            unsafe { from_raw_parts(buffer.as_ptr() as *const f32, BUFFER_SIZE) }.to_owned();
        data = data
            .into_iter()
            .enumerate()
            .map(|(i, f)| f * 0.5 * (1.0 - (2.0 * PI * i as f32 / BUFFER_SIZE as f32).cos())) // hann window
            .collect();
        data.append(&mut vec![0.0; SAMPLE_RATE - BUFFER_SIZE]);

        let mut input: Vec<Complex<f32>> = data
            .into_iter()
            .map(|f| Complex { re: f, im: 0.0 })
            .collect();

        fft.process(&mut input);

        let mut note_map: HashMap<Note, f32> = HashMap::new();

        input
            .iter()
            .map(|v| v.abs())
            .enumerate()
            .take(SAMPLE_RATE / 2) // Skip duplicates
            .filter(|&v| v.1 > 0.75) // Increase to gain certainty
            .map(|f| (f.0.try_into(), f.1))
            .filter_map(|note: (Result<Note, ()>, f32)| {
                if note.0.is_ok() {
                    Some((note.0.unwrap(), note.1))
                } else {
                    None
                }
            })
            .for_each(|note| {
                if !note_map.contains_key(&note.0) {
                    note_map.insert(note.0, note.1);
                } else {
                    let val = note_map.get(&note.0).unwrap();
                    note_map.insert(note.0, note.1 + val);
                }
            });

        eprintln!("{:?}", note_map);
    }
}
